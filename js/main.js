
$(".btn-modal").fancybox({
    'padding'    : 0
});


$('.objectSlider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrows: true,
    focusOnSelect: false,
    prevArrow: '<span class="slide-nav prev"><i class="fas fa-chevron-circle-left"></i></span>',
    nextArrow: '<span class="slide-nav next"><i class="fas fa-chevron-circle-right"></i></span>'
});




(function() {

    $('.checkboxList input[type="radio"]').change(function(){

        var val = '.' + $('.checkboxList input[type="radio"]:checked').val();
        var tab = $(val);
        var box = $('.page');

        console.log(val);

        box.find('.userInfo__tab').removeClass('active');
        box.find(tab).addClass('active');
    });

}());